<?php 

/* Librería: https://code.google.com/p/php-excel/
 * Es una librería muy anticuada, del 2009, sin embargo
 * para demostrar de forma básica la integración de otra
 * librería nos es más que suficiente.
 */
require "aplicacion/librerias/excel/php-excel.class.php";

$xls = new Excel_XML;

$titulos_columnas = array(
	'Titulo Libro',
        'ISBN Libro',
        'Editorial Libro',
        'Anio Publicacion'
);

$xls->addArray(array($titulos_columnas));

foreach ($datos['libros'] as $libro) {
        $aux = array(
    	$libro['titulo_libro'],
        $libro['isbn_libro'],
        $libro['editorial_libro'],
        $libro['anio_publicacion_libro']
    );
    $xls->addArray(array($aux));
}

$xls->generateXML("libros");
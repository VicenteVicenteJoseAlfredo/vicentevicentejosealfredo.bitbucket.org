<!-- los identificadores de los input ya no estan agrupados como en autor en el metodo post-->
<form method="post" action="index.php?c=libros_controller&a=guardar_libro" class="form-horizontal" role="form">

	<div class="form-group">
		<label for="titulo_libro" class="col-md-4">Titulo del libro:
                    <input type="text" placeholder="Ingresa el titulo" class="form-control col-md-8" name="libro[titulo_libro]" value="<?php echo @$datos[libro]['titulo_libro']; ?>" id="titulo_libro" />
		</label>
	</div>

	<div class="form-group">
		<label for="isbn_libro" class="col-md-4">isbn del Libro:
            <input type="text" placeholder="ingresa el isbn" class="form-control col-md-8" name="libro[isbn_libro]" value="<?php echo @$datos[libro]['isbn_libro']; ?>" id="isbn_libro" />
		</label>
	</div>
    
        <div class="form-group">
		<label for="editorial_libro" class="col-md-4">Editorial:
            <input type="text" placeholder="ingresa la editorial" class="form-control col-md-8" name="libro[editorial_libro]"	value="<?php echo @$datos[libro]['editorial_libro']; ?>" id="editorial_libro" />
		</label>
	</div>
    
         <div class="form-group">
		<label for="anio_libro" class="col-md-4">Año publicacion:
            <input type="text" placeholder="ingresa el año" class="form-control col-md-8" name="libro[anio_libro]" value="<?php echo @$datos[libro]['anio_libro']; ?>" id="anio_libro" />
		</label>
	</div>

	<div class="form-group">
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
	</div>

</form>

<?php if (@$datos['error'] == true) { ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">Errores</div>
			<div class="panel-body">
			<ul>
                <?php foreach (@$datos['mensajes_error'] as $error) { ?>
                <li><?php echo $error; ?></li>
                <?php } ?>
			</ul>
			</div>
		</div>
	</div>
</div>
<?php } ?>

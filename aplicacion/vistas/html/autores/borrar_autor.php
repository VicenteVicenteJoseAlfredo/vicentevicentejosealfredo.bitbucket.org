<?php foreach ($datos['autores'] as $autor) { ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		ADVERTECIA!! Se eliminara el autor: <strong><?php echo $autor['nombre_autor']; ?></strong>
	</div>
	<div class="panel-body">
		<ul>
			<li><strong>Nacionalidad:</strong> <?php echo $autor['nacionalidad_autor']; ?></li>
			<li><strong>Más datos:</strong> ...</li>
			<li><strong>Más datos:</strong> ... ¿libros?</li>
			<li><strong>Más datos:</strong> ...</li>
			<li><strong>Más datos:</strong> ... ¿ejemplares?</li>
			<li><strong>Más datos:</strong> ...</li>
			<li><strong>Más datos:</strong> ... ¿qué tanta información de <em>un libro</em>?</li>
			<li><strong>Más datos:</strong> ...</li>
			<li><strong>Más datos:</strong> ... ¿qué tanta información de <em>un ejemplar</em>?</li>
			<li><strong>Más datos:</strong> ...</li>
		</ul>
	</div>

	<div class="panel-footer clearfix">
		<div class="pull-right">
                    <a href="index.php?c=autores&a=confirma_borrar_autor&v=<?php echo $datos['vista']['tipo_vista'];?>&id_autor=<?php echo $autor['id_autor']; ?>" class="btn btn-default">Confirmar</a>
			<a href="index.php?c=autores&a=ver_lista&v=<?php echo $datos['vista']['tipo_vista']; ?>" class="btn btn-warning">Cancelar</a>
		</div>
	</div>
</div>
<?php } ?>

